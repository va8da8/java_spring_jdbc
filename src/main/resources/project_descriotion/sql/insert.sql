-- добавляем данные в таблицу книги
insert into books(title, author, date_added)
values ('Недоросль', 'Д. И. Фонвизин', now());

insert into books(title, author, date_added)
values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев',
        now() - interval '24h');

insert into books
values (3, 'Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');

insert into books
values (4, 'Сестра моя - жизнь', 'Б. Л. Пастернак', now());


-- изменяем максимальное кол-во символов в столбце
alter table books
alter column title type varchar(100);