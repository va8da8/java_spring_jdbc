-- создаем таблицу книга
create table books(
    id         serial primary key,
    title      varchar(30) NOT NULL,
    author     varchar(30) not null,
    date_added timestamp   not null
);


-- создаем таблицу клиента
create table users (
    id bigserial primary key,
    first_name varchar(30) not null,
    last_name varchar(30) not null,
    birth_date date not null,
    phone_number varchar(30) not null,
    email varchar(30) not null,
    books_title varchar(100)
);


select *
from users;