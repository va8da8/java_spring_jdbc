package ru.project.jdbc;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.project.jdbc.db_example.MyDBApplicationContext;
import ru.project.jdbc.db_example.dao.UserDAOBean;
import ru.project.jdbc.db_example.model.User;

import java.time.LocalDate;


// для запуска кода реализуем интерфейс CommandLineRunner
@SpringBootApplication
public class Application implements CommandLineRunner {


    // пример: 4
    /*
    private BookDAOBean bookDAOBean;


    @Autowired // создаем Bean класса BookDAOBean
    public void setBookDAOBean(BookDAOBean bookDAOBean) {

        this.bookDAOBean = bookDAOBean;
    }
    */


    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }


    // и переопределяем метод
    @Override
    public void run(String... args) throws Exception {

        //пример: 1,2
        /*
        // подключаемся к БД и выводим книгу по Id = 1
        BookDAOJDBC bookDAOJDBC = new BookDAOJDBC();
        bookDAOJDBC.findBookById(1);
        */

        // пример: 3
        /*
        ApplicationContext ctx = new AnnotationConfigApplicationContext
                (MyDBApplicationContext.class);

        BookDAOBean bookDAOBean = ctx.getBean(BookDAOBean.class);
        bookDAOBean.findBookById(1);
        */

        // пример: 4
        /*
        bookDAOBean.findBookById(1);
        */

        ApplicationContext ctx = new AnnotationConfigApplicationContext
                (MyDBApplicationContext.class);

        UserDAOBean userDAOBean = ctx.getBean(UserDAOBean.class);

        User user_1 = new User();
        user_1.setFirstName("Петя");
        user_1.setLastName("Петров");
        user_1.setBirthDate(
                LocalDate.of(2000, 01, 01));
        user_1.setPhoneNumber("8-888-888-88-88");
        user_1.setEmail("petia@yandex.ru");
        user_1.setBookTitle("Доктор Живаго, Недоросль");
        userDAOBean.addUser(user_1);

        User user_2 = new User();
        user_2.setFirstName("Иван");
        user_2.setLastName("Иванов");
        user_2.setBirthDate(
                LocalDate.of(2010, 10, 10));
        user_2.setPhoneNumber("1-111-111-11-11");
        user_2.setEmail("ivan@rambler.ru");
        user_2.setBookTitle("Путешествие из Петербурга в Москву");
        userDAOBean.addUser(user_2);

        userDAOBean.getBook("petia@yandex.ru");
    }
}