package ru.project.jdbc.db_example.db;


import ru.project.jdbc.db_example.constance.DBConstance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


// реализуем паттерн Singleton
public enum DBApp implements DBConstance {

    INSTANCE;

    private Connection connection;


    public Connection newConnection() throws SQLException {

        if (connection == null) {

            connection = DriverManager.getConnection
                    // прописываем подключение
//                    ("jdbc:postgresql://localhost:5432/local_db_jdbc",
//                            "postgres", "12345");
        ("jdbc:postgresql://" +
                DB_HOST + ":" + PORT + "/" + DB, USER, PASSWORD);
        }
        return connection;
    }
}