package ru.project.jdbc.db_example.constance;


public interface DBConstance {

    String DB_HOST = "localhost";
    String DB = "local_db_jdbc";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}