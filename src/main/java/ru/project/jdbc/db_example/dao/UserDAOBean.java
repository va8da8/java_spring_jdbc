package ru.project.jdbc.db_example.dao;


import ru.project.jdbc.db_example.model.Book;
import ru.project.jdbc.db_example.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class UserDAOBean {

    private final Connection connection;


    public UserDAOBean(Connection connection) {

        this.connection = connection;
    }


    /**
     * Метод addUser(User user), который добавляет данные в таблицу
     * users согласно своим полям.
     */
    public void addUser(User user) throws SQLException {

        PreparedStatement insertUpdate = connection.prepareStatement
                ("insert into users (first_name, last_name, " +
                        "birth_date, phone_number, email, " +
                        "books_title) " +
                        "values (?, ?, ?, ?, ?, ?)");
        insertUpdate.setString(1, user.getFirstName());
        insertUpdate.setString(2, user.getLastName());
        insertUpdate.setDate(3,
                java.sql.Date.valueOf(user.getBirthDate()));
        insertUpdate.setString(4, user.getPhoneNumber());
        insertUpdate.setString(5, user.getEmail());
        insertUpdate.setString(6, user.getBookTitle());
        insertUpdate.executeUpdate();
    }


    /**
     * Метод getBook(String email), который получает на вход значение
     * email и сохраняет список книг пользователя соответствующего этому
     * значению. Используя метод findBook(String bookTitle) проходим
     * по списку и выводим данные из таблицы books.
     */
    public void getBook(String email) throws SQLException {

        PreparedStatement selectQuery = connection.prepareStatement
                ("select * from users where email = ?");
        selectQuery.setString(1, email);
        ResultSet resultSet = selectQuery.executeQuery();

        User user = new User();
        while (resultSet.next()) {
            user.setBookTitle(resultSet.getString
                    ("books_title"));
        }
        List<String> list = List.of(user.getBookTitle().split(", "));

        for (String s : list) {
            findBook(s);
        }
    }


    /**
     * Метод findBook(String bookTitle), который получает на вход
     * название книги и выводит все данные о ней.
     */
    public void findBook(String bookTitle) throws SQLException {

        PreparedStatement selectQuery = connection.prepareStatement
                ("select * from books where title = ?");
        selectQuery.setString(1, bookTitle);

        ResultSet resultSet = selectQuery.executeQuery();

        Book book = new Book();
        while (resultSet.next()) {
            book.setBookId(resultSet.getInt("id"));
            book.setBookAuthor(resultSet.getString("author"));
            book.setBookTitle(resultSet.getString("title"));
            book.setDateAdded(resultSet.getDate("date_added"));
            System.out.println(book);
        }
    }
}