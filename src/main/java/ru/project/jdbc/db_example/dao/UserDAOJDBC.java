//package ru.sber.spring.db_example.dao;
//
//
//import ru.sber.spring.db_example.db.DBApp;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.SQLException;
//
//
//public class UserDAOJDBC {
//
//
//    public void addUser(String firstName, String lastName, String birthDate,
//                        String phoneNumber, String email, Integer bookId) {
//
//        try (Connection connection = DBApp.INSTANCE.newConnection()) {
//
//            if (connection != null) {
//                System.out.println("подключились к БД");
//            }
//            else {
//                System.out.println("неподключились к БД");
//            }
//
//            PreparedStatement insertUpdate = connection.prepareStatement
//                    ("insert into users (first_name, last_name, " +
//                            "birth_date, phone_number, email, books_id) " +
//                            "values (?, ?, ?, ?, ?, ?)");
//            insertUpdate.setString(1, firstName);
//            insertUpdate.setString(2, lastName);
//            insertUpdate.setString(3, birthDate);
//            insertUpdate.setString(4, phoneNumber);
//            insertUpdate.setString(5, email);
//            insertUpdate.setInt(6, bookId);
//            insertUpdate.executeUpdate();
//
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//    }
//
//
////    public User findUserById(Integer userId) {
////
////        try (Connection connection = DBApp.INSTANCE.newConnection()) {
////            if (connection != null) {
////                System.out.println("подключились к БД");
////            }
////            else {
////                System.out.println("неподключились к БД");
////            }
////
////            PreparedStatement selectQuery = connection.prepareStatement
////                    ("select * from users where id = ?");
////            ResultSet resultSet = selectQuery.executeQuery();
////
////        } catch (SQLException e){
////            System.out.println(e.getMessage());
////        }
////        return null;
////    }
//}