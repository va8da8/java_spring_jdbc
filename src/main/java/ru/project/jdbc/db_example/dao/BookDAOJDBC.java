package ru.project.jdbc.db_example.dao;


import ru.project.jdbc.db_example.db.DBApp;
import ru.project.jdbc.db_example.model.Book;

import java.sql.*;


public class BookDAOJDBC {


    // пример: 2
    //JDBC - Java DataBase Connection
    public Book findBookById(Integer bookId) {
        // подключаемся к БД
        try (Connection connection = DBApp.INSTANCE.newConnection()) {

            if (connection != null) {
                System.out.println("ура! мы подключились к БД!!!!");
            } else {
                System.out.println("база данных отдыхает, не трогайте!");
            }
            // создаем выражение SQL
            PreparedStatement selectQuery = connection.prepareStatement
                    ("select * from books where id = ?");
            // подставляем значение вместо ?, где 1 - первый знак ?
            selectQuery.setInt(1, bookId);
            ResultSet resultSet = selectQuery.executeQuery();
            while (resultSet.next()) {

                Book book = new Book();

                // вытягиваем данные из таблицы в БД по названиям столбцов
                book.setBookId(resultSet.getInt("id"));

                book.setBookTitle(resultSet.getString
                        ("title"));

                book.setBookAuthor(resultSet.getString
                        ("author"));

                book.setDateAdded(resultSet.getDate
                        ("date_added"));

                System.out.println(book);
                return book;
            }
        } catch (SQLException e) {
            System.out.println("Error:" + e.getMessage());
        }
        // если нет книги с данным Id вернется null
        return null;
    }


    // пример: 1
/*
    public Book findBookById(Integer bookId) {
        // подключаемся к БД
        try (Connection connection = newConnection()) {
            if (connection != null) {
                System.out.println("ура! мы подключились к БД!!!!");
            } else {
                System.out.println("база данных отдыхает, не трогайте!");
            }
            // создаем выражение SQL
            PreparedStatement selectQuery = connection.prepareStatement
                    ("select * from books where id = ?");
            // подставляем значение вместо ?, где 1 - первый знак ?
            selectQuery.setInt(1, bookId);
            ResultSet resultSet = selectQuery.executeQuery();
            while (resultSet.next()) {

                Book book = new Book();

                // вытягиваем данные из таблицы в БД по названиям
                // столбцов
                book.setBookId(resultSet.getInt("id"));

                book.setBookTitle(resultSet.getString
                        ("title"));

                book.setBookAuthor(resultSet.getString
                        ("author"));

                book.setDateAdded(resultSet.getDate
                        ("date_added"));

                System.out.println(book);
                return book;
            }
        } catch (SQLException e) {
            System.out.println("Error:" + e.getMessage());
        }
        // если нет книги с данным Id вернется null
        return null;
    }


    // устанавливаем соединение с БД
    private Connection newConnection() throws SQLException {

        return DriverManager.getConnection
                ("jdbc:postgresql://localhost:5432/local_db_jdbc",
                        "postgres",
                        "12345");
    }
*/
}
