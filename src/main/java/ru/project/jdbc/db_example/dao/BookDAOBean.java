package ru.project.jdbc.db_example.dao;


import ru.project.jdbc.db_example.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


//@Component // пример: 4
public class BookDAOBean {

    private final Connection connection;


    public BookDAOBean(Connection connection) {

        this.connection = connection;
    }


    // пример: 3
    public Book findBookById(Integer bookId) throws SQLException {

        PreparedStatement selectQuery = connection.prepareStatement
                ("select * from books where id = ?");
        selectQuery.setInt(1, bookId);
        ResultSet resultSet = selectQuery.executeQuery();

        Book book = new Book();
        while (resultSet.next()) {

            book.setBookId(resultSet.getInt("id"));
            book.setBookAuthor(resultSet.getString("author"));
            book.setBookTitle(resultSet.getString("title"));
            book.setDateAdded(resultSet.getDate("date_added"));
            System.out.println(book);
        }
        return book;
    }
}