package ru.project.jdbc.db_example;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.project.jdbc.db_example.dao.BookDAOBean;
import ru.project.jdbc.db_example.dao.UserDAOBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.project.jdbc.db_example.constance.DBConstance.*;


@Configuration
//@ComponentScan // пример: 4
public class MyDBApplicationContext {


    @Bean
    // Т.к. мы подключаемся к одной БД мы создаем один экземпляр
    // подключения
    @Scope("singleton")
    public Connection getConnection() throws SQLException {

        return DriverManager.getConnection("jdbc:postgresql://" +
                DB_HOST + ":" + PORT + "/" + DB, USER, PASSWORD);
    }


    // для примера: 4, нужно закомментировать код ниже
    @Bean
    public BookDAOBean bookDAOBean() throws SQLException {

        return new BookDAOBean(getConnection());
    }


    @Bean
    @Scope("prototype")
    public UserDAOBean userDAOBean() throws SQLException {

        return new UserDAOBean(getConnection());
    }
}