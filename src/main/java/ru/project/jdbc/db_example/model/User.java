package ru.project.jdbc.db_example.model;


import lombok.*;

import java.time.LocalDate;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Setter(AccessLevel.NONE)
    private Integer userId;

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

    private String phoneNumber;

    private String email;

    private String bookTitle;
}