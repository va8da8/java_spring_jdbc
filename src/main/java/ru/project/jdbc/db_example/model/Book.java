package ru.project.jdbc.db_example.model;


import lombok.*;

import java.util.Date;


// создает методы -get и -set и переписывает hashcode и equals
//@Data
@Getter
@Setter
@ToString
// конструктор без аргументов
@NoArgsConstructor
// конструктор со всеми полями
@AllArgsConstructor
// POJO - Plain Old Java Object(класс описывающий конкретный,
// простой объект)
public class Book {

    /*
     * создаем поля(столбцы) таблицы
     */
    // запрещает метод -set для данного поля
    //@Setter(AccessLevel.NONE)
    private Integer bookId;

    private String bookTitle;

    private String bookAuthor;

    private Date dateAdded;
}